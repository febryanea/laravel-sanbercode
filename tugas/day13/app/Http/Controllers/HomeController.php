<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function master(){
        return  view('master');
    }

    public function table(){
        return  view('table');
    }

    public function datatables(){
        return  view('data-tables');
    }

}
