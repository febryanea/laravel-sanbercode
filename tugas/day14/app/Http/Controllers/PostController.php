<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function master(){
        return  view('master');
    }

    public function table(){
        return  view('table');
    }

    public function datatables(){
        return  view('data-tables');
    }

    public function index() {
        $casts = DB::table('cast')->get();
        // dd($casts);
        return  view('index', compact('casts'));
    }

    public function create(){
        return  view('create');
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')->insert([
            "name" => $request["name"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast')->with('success', 'Cast Berhasil di Tambah'); 
    }

    public function show($id){
        $detail = DB::table('cast')->where('id', $id)->first();
        return  view('show', compact('detail'));
    }
    
    public function edit($id){
        $detail = DB::table('cast')->where('id', $id)->first();
        return  view('edit', compact('detail'));
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
                ->where('id', $id)
                ->update([
                    'name' => $request['name'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]);
                return redirect('/cast')->with('success', 'Data Berhasil di Update'); 
    }

    public function destroy($id){
        $query = DB::table('cast')->where('id', $id)->delete();
        return  redirect('/cast')->with('success', 'Cast berhasil di hapus');
    }
}
