<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/master', 'PostController@master');
Route::get('/table', 'PostController@table');
Route::get('/data-tables', 'PostController@datatables');

Route::get('/cast', 'PostController@index');
Route::get('/cast/create', 'PostController@create');
Route::post('/cast', 'PostController@store');
Route::get('/cast/{id}', 'PostController@show');
Route::get('/cast/{id}/edit', 'PostController@edit');
Route::put('/cast/{id}', 'PostController@update');
Route::delete('/cast/{id}', 'PostController@destroy');