@extends('master')


@section('content')
              <div class="card-header">
                <h3 class="card-title">Add Cast</h3>
              </div>
              <form id="castform" action="/cast/{{ $detail->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" value=" {{ old('name', $detail->name) }} "placeholder="Name" required>
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="umur">Age</label>
                    <input type="number" name="umur" class="form-control" value="{{ $detail->umur }}"placeholder="Umur" required>
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="bio">Bio</label><br>
                    <textarea type="text" name="bio" class="form-control" placeholder="Text" required>{{ $detail->bio }}</textarea>
                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                  <a class="btn btn-info" href="/cast">Cancel</a>
                </div>
              </form>

@endsection