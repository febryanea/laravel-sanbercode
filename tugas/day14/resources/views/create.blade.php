@extends('master')


@section('content')
              <div class="card-header">
                <h3 class="card-title">Add Cast</h3>
              </div>
              <form id="castform" action="/cast" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" value=" {{ old('name', '') }} "placeholder="Name" required>
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="umur">Age</label>
                    <input type="number" name="umur" class="form-control" placeholder="Umur" required>
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="bio">Bio</label><br>
                    <textarea name="bio" class="form-control" placeholder="Text" required></textarea>
                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>

@endsection