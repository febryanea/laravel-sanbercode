@extends('master')

@section('content')
<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h1 class="card-title">{{ $detail->name }} ({{ $detail->umur }})</h1><br>
    <br>
    <p class="card-text">{{ $detail->bio }}</p>
    <a href="/cast" class="btn btn-primary">back</a>
  </div>
</div>
@endsection
