@extends('master')

@section('content')
<div class="card">
      <div class="card-header">
        <h3 class="card-title">DataTable Cast</h3>
      </div>
      <div class="card-body">
          @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
          @endif
        <a class="btn btn-info" href="/cast/create">New Cast</a>
        <table id="cast" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th style="width: 50px">No</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th style="width: 300px">Detail</th>
          </tr>
          </thead>
          <tbody>
              @foreach($casts as $key)
                <tr>
                    <td> {{ $key->id}}</td>
                    <td> {{ $key->name }}</td>
                    <td> {{ $key->umur }}</td>
                    <td> {{ $key->bio }}</td>
                    <td> 
                    <form action="/cast/{{$key->id}}" method="post">
                    <a class="btn btn-primary" href="/cast/{{ $key->id }}">show</a>
                    <a class="btn btn-info" href="/cast/{{ $key->id }}/edit">edit</a>
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger" value="delete">
                    </form>
                    </td>
                </tr>
              @endforeach
          </tbody>
        </table>
      </div>
</div>
@endsection

@push('scripts')

<script src="{{ asset('/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#cast").DataTable();
  });
</script>

@endpush