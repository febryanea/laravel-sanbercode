<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('home');
    }

    public function form(){
        return  view('form');
    }

    public function welcome(Request $request){
        $nama_depan = $request['first_name'];
        $nama_belakang = $request['last_name'];
        return  view('welcome', compact('nama_depan', 'nama_belakang'));
    }
}
